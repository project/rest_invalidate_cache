<?php

namespace Drupal\rest_invalidate_cache\Plugin\rest\resource;

use Drupal\Core\Cache\Cache;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a REST resource to invalidate cache tags.
 *
 * @RestResource(
 *   id = "rest_invalidate_cache_invalidate",
 *   label = @Translation("Invalidate cache"),
 *   uri_paths = {
 *     "create" = "/invalidate_cache/{tags}"
 *   }
 * )
 */
class InvalidateCache extends ResourceBase {

  /**
   * Invalidates the specified cache tags.
   *
   * @param string $tags
   *   A comma-separated list of cache tags to invalidate.
   *
   * @return \Drupal\rest\ResourceResponse
   *   A response object containing the result message.
   */
  public function post(string $tags): ResourceResponse {
    $tagList = explode(',', $tags);
    Cache::invalidateTags($tagList);

    return new ResourceResponse(
      [
        'message' => $this->t(
          'Tags invalidated: @tags',
          ['@tags' => implode(', ', $tagList)]
        ),
      ]
    );
  }

}
